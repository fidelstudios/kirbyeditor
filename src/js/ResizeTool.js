define("editor/ResizeTool", ["editor/Tool", "Arstider/Keyboard", "Arstider/Mouse"], function(Tool, Keyboard, Mouse){

	function ResizeTool(editor){
		Arstider.Super(this, Tool, editor);
	}

	Arstider.Inherit(ResizeTool, Tool);

	ResizeTool.prototype.update = function() {
	};


	return ResizeTool;
});