define("editor/DialogEnum", [], function(){

	function DialogEnum(){}

	DialogEnum.TOOLS = 1;
	DialogEnum.TEMPLATES = 2;
	DialogEnum.LAYERS = 3;
	DialogEnum.LEVELS = 4;
	DialogEnum.ENTITY = 5;

	return DialogEnum;
});