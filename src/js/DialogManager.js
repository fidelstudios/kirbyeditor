define("editor/DialogManager", ["editor/Dialog"], function(Dialog){

	function DialogManager(editor){
		this.editor = editor;
		this.container = document.createElement("div");
		document.getElementById("Arstider_tag_overlay").remove()

		this.dialogs = {};
		this.registered = {};
	}

	DialogManager.prototype.register = function(id, func){
		this.registered[id] = func;
	}

	DialogManager.prototype.get = function(id){
		return this.dialogs[id];
	}

	DialogManager.prototype.create = function(id){
		var dialog = this.dialogs[id];
		if(dialog){
			return dialog;
		}

		var func = this.registered[id];
		if(!func){
			console.error("No class for dialog " + id);
			func = Dialog;
		}

		var dialog = new func();
		dialog.editor = this.editor;
		this.dialogs[id] = dialog;

		dialog.onCreated();

		return dialog;
	}

	DialogManager.prototype.destroy = function(id){
		var dialog = this.get(dialog);
		if(!dialog){
			return;
		}

		$(dialog).window('destroy');
		delete this.dialogs[id];
	}

	DialogManager.prototype.open = function(id){
		this.create(id);
	}

	DialogManager.prototype.close = function(id){
		this.destroy(id);
	}

	DialogManager.prototype.update = function(){
		for (var a in this.dialogs) {
			this.dialogs[a].update();
		};
	}

	return DialogManager;
});