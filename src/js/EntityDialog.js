define("editor/EntityDialog", ["editor/Editor", "editor/Dialog", "editor/DialogEnum"], function(Editor, Dialog, DialogEnum){

	function EntityDialog(){
		this.entity = null;

		var props = {
			id:DialogEnum.ENTITY,
			title:"Entity",
			width:300,
			height:400,
			collapsible:false,
			minimizable:false,
			maximizable:false,
			closable:false
		};

		Arstider.Super(this, Dialog, props);
	}

	Arstider.Inherit(EntityDialog, Dialog);

	EntityDialog.prototype.onCreated = function(){
		var container = $(this.container);

		var propertyGrid = this.propertyGrid = $('<table></table>'); container.append(propertyGrid);
		propertyGrid.propertygrid({
			width:"100%",
			height:"100%",
			scrollbarSize:0,
			showGroup: true,
			data:[],
			onAfterEdit:this.onAfterEdit.bind(this)
		});

		//var entities = this.editor.gateway.getWorld().entities;
		//var entity = entities[Math.floor(Math.random()*entities.length)]
		//var entity = this.editor.gateway.getPlayer();
		//this.setEntity(entity);
	};

	EntityDialog.prototype.update = function(){
		this.setEntity(this.editor.selectedElement);
	};

	EntityDialog.prototype.setEntity = function(entity){
		if(this.entity == entity)
			return;

		this.entity = entity;

		var data = {total:0, rows:[]};

		if(entity){
			for (var i = entity.components.length - 1; i >= 0; i--) {
				var component = entity.components[i];
				var props = component.constructor.DEFAULTS || {};

				for(var a in props){
					//if(!this.editor.gateway.filterProperties(pr, a))
					//	continue;
					var value = (component[a] != undefined ? component[a] : props[a]);
					var stringify = false;
					if(value instanceof Object){
						stringify = true;
						value = JSON.stringify(value);
					}
					data.rows.push({name:a, value:value, group:component.componentName, editor:"text", stringify:stringify});
				}
			};
		}

		this.propertyGrid.propertygrid('loadData', data);
	}

	EntityDialog.prototype.onAfterEdit = function(rowIndex, rowData, changes){
		if(!changes || changes.value == undefined || changes.value == null){
			return;
		}

		var value = changes.value;

		if(!isNaN(Number(value))){
			value = Number(value);
		}

		if(value == "false"){
			value = false;
		}
		else if(value == "true"){
			value = true;
		}


		if(rowData.stringify){
			value = JSON.parse(value);
		}
		this.entity[rowData.group][rowData.name] = value;

		var gateway = this.editor.gateway;
		var data = gateway.saveEntity(this.entity);
		gateway.removeElement(this.entity);
		var entity = gateway.createEntity(data, this.entity.templateName);
		this.editor.selectedElement = entity;
		//this.entity[rowData.group][rowData.name] = value;
		//this.editor.gateway.updateElementPosition(this.entity, this.entity.transform.position.x, this.entity.transform.position.y);

	};

	return EntityDialog;
});