define("editor/MoveTool", ["editor/Tool", "Arstider/Keyboard", "Arstider/Mouse"], function(Tool, Keyboard, Mouse){

	function MoveTool(editor){
		Arstider.Super(this, Tool, editor);
		this.dragger = false;
		this.snap = 10;
	}

	Arstider.Inherit(MoveTool, Tool);

	MoveTool.prototype.update = function() {
		var selectedElement = this.editor.selectedElement;

		if(Mouse.isPressed() && selectedElement){

			var xMouse = Mouse.x();
			var yMouse = Mouse.y();

			if(!this.dragger){
				this.dragger = {
					x:xMouse,
					y:yMouse,
					xOffset:selectedElement.transform.position.x - xMouse,
					yOffset:selectedElement.transform.position.y - yMouse,
					enabled:false
				}
			}
			else if(this.dragger.enabled){
				this.dragElement(selectedElement, xMouse, yMouse);
			}
			else{
				var dist = Math.sqrt(Math.pow(xMouse - this.dragger.x, 2) + Math.pow(yMouse - this.dragger.y, 2))
				if(dist > 10)
					this.dragger.enabled = true;
			}
		}
		else{
			this.dragger = false;
		}
	};


	MoveTool.prototype.dragElement = function(element, x, y){
		x += this.dragger.xOffset;
		y += this.dragger.yOffset;

		if(!element){
			return;
		}

		if(!Keyboard.getKey("shift")){
			x = Math.round(x / this.snap) * this.snap;
			y = Math.round(y / this.snap) * this.snap;
		}
		this.editor.gateway.updateElementPosition(element, x, y)

	};

	return MoveTool;
});