define("editor/Editor", [
	"Arstider/Engine",
	"Arstider/Mouse",
	"Arstider/Keyboard",
	"editor/DialogManager",
	"editor/DialogEnum",
	"editor/ToolsDialog",
	"editor/TemplatesDialog",
	"editor/EntityDialog",
	"editor/LevelsDialog",
	"editor/EditorGateway"
],
function(Engine, Mouse, Keyboard, DialogManager, DialogEnum, ToolsDialog, TemplatesDialog, EntityDialog, LevelsDialog, EditorGateway){

	Editor.PATH = "/KirbyEditor/";

	function Editor(){
		this.edit = false;
		this.container = null;
		this.selectedVisual = null;
		this.selectedElement = null;
		this.tool = null;
		this.gateway = EditorGateway;
		this.cameraPosition = {x:0, y:0};

		this.loadCss(Editor.PATH + "libs/easyui/themes/default/easyui.css");
		this.loadCss(Editor.PATH + "libs/easyui/themes/icon.css");
		require([
			Editor.PATH + "libs/easyui/jquery.min.js",
			Editor.PATH + "libs/easyui/jquery.easyui.min.js"
		], this.init.bind(this));
	};

	Editor.prototype.loadCss = function(url) {
	    var link = document.createElement("link");
	    link.type = "text/css";
	    link.rel = "stylesheet";
	    link.href = url;
	    document.getElementsByTagName("head")[0].appendChild(link);
	};

	Editor.prototype.init = function() {
		// Init dom container
		this.container = document.createElement("div");
		document.body.appendChild(this.container);

		// Init the window manager
		this.dialogs = new DialogManager(this);
		this.dialogs.register(DialogEnum.TOOLS, ToolsDialog);
		this.dialogs.register(DialogEnum.TEMPLATES, TemplatesDialog);
		this.dialogs.register(DialogEnum.ENTITY, EntityDialog);
		this.dialogs.register(DialogEnum.LEVELS, LevelsDialog);
		this.container.appendChild(this.dialogs.container);

		// Init binding
		Keyboard.bind("left", "down", this.scrollCamera.bind(this, -1,0));
		Keyboard.bind("right", "down", this.scrollCamera.bind(this, 1,0));
		Keyboard.bind("up", "down", this.scrollCamera.bind(this, 0,-1));
		Keyboard.bind("down", "down", this.scrollCamera.bind(this, 0,1));

		// Show the default dialog
		this.dialogs.open(DialogEnum.TOOLS);
		this.dialogs.open(DialogEnum.TEMPLATES);
		this.dialogs.open(DialogEnum.ENTITY);
		this.dialogs.open(DialogEnum.LEVELS);

		// Init infinite loop
		this.update();
	};

	Editor.prototype.levelLoaded = function() {
		this.editMode(true);
	};

	Editor.prototype.editMode = function(bool){
		this.edit = bool;
		this.gateway.editMode(bool);

        if(!this.edit){
            Mouse._touchRelay = Engine.applyTouch;
            //Arstider.skipUpdate = false;
        }
        else{
            Mouse._touchRelay = this.findClickedElement.bind(this);
            //Arstider.skipUpdate = true;
        }
	}

	Editor.prototype.scrollCamera = function(x, y){
		if(!this.edit)
			return;

		var f = (Keyboard.getKey("shift") ? 200 : 50);
		this.gateway.scrollCamera(x * f, y * f);
	};

	Editor.prototype.update = function(){
		this.dialogs.update();

		if(this.edit && this.tool){
			this.tool.update();
		}
		window.requestAnimationFrame(this.update.bind(this));
	};


	Editor.prototype.findClickedElement = function(){
        var clickedElements = this.browseDisplayElements(Engine.currentScreen);

        for (var i = 0; i < clickedElements.length; i++) {
        	var visual = clickedElements[i];
        	if(!visual.alpha)
        		continue;
        	var element = this.gateway.filterElement(visual);
        	if(element){
        		this.selectElement(element, visual);
        		return element;
        	}
        };
        return null;
    };

	Editor.prototype.browseDisplayElements = function(e){
		var elements = [];
        for(var input = 0; input < 1; input++){
        	if(e.isTouched(Mouse.x(input), Mouse.y(input)) && Mouse.isPressed(input)){
        		elements.push(e);
        	}
        }

        if(e.children){
        	for (var i = e.children.length - 1; i >= 0; i--) {
        		elements = elements.concat(this.browseDisplayElements(e.children[i]));
        	};
        }
        return elements;
	};

	Editor.prototype.selectElement = function(element, visual){
		if(this.selectedVisual){
			this.selectedVisual.showOutline = false;
		}

		this.selectedVisual = visual;
		this.selectedElement = element;

		if(visual){
			visual.showOutline = true;
		}
	};

	Editor.prototype.resetSelection = function(){
		this.selectElement();
	};

	Editor.prototype.setTool = function(toolClass){
		this.tool = new toolClass(this);
	};

	return new Editor();
});





