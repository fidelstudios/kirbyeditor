define("editor/ToolsDialog", ["editor/Dialog", "editor/DialogEnum", "editor/MoveTool", "editor/DeleteTool"], function(Dialog, DialogEnum, MoveTool, DeleteTool){

	function ToolsDialog(){

		var props = {
			id:DialogEnum.TOOLS,
			title:"Tools",
			width:200,
			height:48,
			collapsible:false,
			minimizable:false,
			maximizable:false,
			closable:false,
			resizable:false
		};

		Arstider.Super(this, Dialog, props);
	}

	Arstider.Inherit(ToolsDialog, Dialog);

	ToolsDialog.prototype.onCreated = function(){
		var container = $(this.container);

		this.moveBtn = $("<a></a>");
		this.moveBtn.linkbutton({
			toggle: true,
			text:"Move",
    		iconCls: 'icon-reload',
    		onClick:this.onMoveBtn.bind(this)
		});
		container.append(this.moveBtn);

		this.deleteBtn = $("<a></a>");
		this.deleteBtn.linkbutton({
			toggle: true,
			text:"Delete",
    		iconCls: 'icon-cancel',
    		onClick:this.onDeleteBtn.bind(this)
		});
		container.append(this.deleteBtn);

		// Select move as default tool
		this.onMoveBtn();
	};

	ToolsDialog.prototype.select = function(btn){
		var buttons = [this.deleteBtn, this.moveBtn];
		for (var i = 0; i < buttons.length; i++) {
			if(buttons[i] == btn) continue;
			$(buttons[i]).linkbutton('unselect');
		};
	}

	ToolsDialog.prototype.onDeleteBtn = function(){
		this.editor.setTool(DeleteTool);
		this.select(this.deleteBtn);
	};

	ToolsDialog.prototype.onMoveBtn = function(){
		this.editor.setTool(MoveTool);
		this.select(this.moveBtn);
	};

	return ToolsDialog;
});