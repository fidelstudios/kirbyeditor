define("editor/DeleteTool", ["editor/Tool", "Arstider/Keyboard", "Arstider/Mouse"], function(Tool, Keyboard, Mouse){

	function DeleteTool(editor){
		Arstider.Super(this, Tool, editor);
		this.cooldown = 0;
	}

	Arstider.Inherit(DeleteTool, Tool);

	DeleteTool.prototype.update = function() {
		if(Mouse.isPressed() && this.cooldown < 0){
			var element = this.editor.findClickedElement();
			if(element){
				this.editor.gateway.removeElement(this.editor.selectedElement);
				this.editor.resetSelection();
				this.cooldown = 60;
			}
		}
		this.cooldown--;
	};

	return DeleteTool;
});