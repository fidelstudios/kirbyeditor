define("editor/TemplatesDialog", ["editor/Editor", "editor/Dialog", "editor/DialogEnum"], function(Editor, Dialog, DialogEnum){

	function TemplatesDialog(){

		var props = {
			id:DialogEnum.TEMPLATES,
			title:"Templates",
			width:200,
			height:500,
			collapsible:false,
			minimizable:false,
			maximizable:false,
			closable:false
		};

		Arstider.Super(this, Dialog, props);
	}

	Arstider.Inherit(TemplatesDialog, Dialog);

	TemplatesDialog.prototype.onCreated = function(){
		var container = $(this.container);

		// Retrieve template data
		var templates = this.editor.gateway.getTemplates();
		var entityTemplates = [];
		for (var a in templates.entities) {
			var template = templates.entities[a];
			if(!template.tags || template.tags.indexOf("editor") == -1){
				continue;
			}
			template.name = a;
			entityTemplates.push(template);
		};

		var dg = $('<table></table>'); container.append(dg);
		dg.datagrid({
			onDblClickRow:this.onDblClickRow.bind(this),
			singleSelect:true,
			fitColumns:true,
			width:"100%",
			height:"100%",
			data:entityTemplates,
			columns:[[{field:"name", title:"Name"}]]
		});
	};

	TemplatesDialog.prototype.onDblClickRow = function(rowIndex, rowData){
		var position = this.editor.gateway.getScreenCenterPosition();

		this.editor.gateway.createEntity({components:{transform:{position:{x:position.x,y:position.y - 100}}}}, rowData.name);
	};

	return TemplatesDialog;
});