define("editor/Dialog", ["Arstider/core/Storage"], function(Storage){

	function Dialog(props){

		// Default props
		this.props = {
			id:-1,
			title:"Editor dialog",
		    width:300,
		    height:300,
		    draggable:true,
		    resizable:true,
		    shadow:false,
		    onMove:this.onMove.bind(this),
		    onResize:this.onResize.bind(this)
		}

		// Override props
		for(var a in props){
			this.props[a] = props[a];
		}

		// Set the dialog id
		this.editor = this.props.editor;
		this.id = this.props.id;

		// Load saved props
		this.loadProps();
		for(var a in this.storage){
			this.props[a] = this.storage[a];
		}

		// Create the dialog element
		this.container = $('<div>');

		// Initialize the dialog
		$(this.container).window(this.props);
	}

	Dialog.STORAGE_KEY = "editor_dialog";

	Dialog.prototype.update = function(){}

	Dialog.prototype.onCreated = function(){}

	Dialog.prototype.onMove = function(left, top){
		this.storage.left = left;
		this.storage.top = top;
		this.saveProps();
	}

	Dialog.prototype.onResize = function(width, height){
		this.storage.width = width;
		this.storage.height = height;
		this.saveProps();
	}

	Dialog.prototype.loadProps = function(){
		var data = Storage.get(Dialog.STORAGE_KEY + this.id);
		this.storage = data ? JSON.parse(data) : {};
	}

	Dialog.prototype.saveProps = function(){
		var data = JSON.stringify(this.storage);
		Storage.set(Dialog.STORAGE_KEY + this.id, data);
	}

	return Dialog;
});