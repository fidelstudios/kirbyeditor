define("editor/LevelsDialog", ["editor/Dialog", "editor/DialogEnum"], function(Dialog, DialogEnum){

	function LevelsDialog(){

		var props = {
			id:DialogEnum.LEVELS,
			title:"Levels",
			width:200,
			height:48,
			collapsible:false,
			minimizable:false,
			maximizable:false,
			closable:false,
			resizable:false
		};

		Arstider.Super(this, Dialog, props);
	}

	Arstider.Inherit(LevelsDialog, Dialog);

	LevelsDialog.prototype.onCreated = function(){
		var container = $(this.container);

		this.editBtn = $("<a></a>");
		this.editBtn.linkbutton({
			toggle: true,
			selected:true,
			text:"Edit",
    		iconCls: 'icon-edit',
    		onClick:this.onEditBtn.bind(this)
		});
		container.append(this.editBtn);

		this.newBtn = $("<a></a>");
		this.newBtn.linkbutton({
			text:"New",
    		iconCls: 'icon-add',
    		onClick:this.onNewBtn.bind(this)
		});
		container.append(this.newBtn);

		this.saveBtn = $("<a></a>");
		this.saveBtn.linkbutton({
			text:"Save",
    		iconCls: 'icon-save',
    		onClick:this.onSaveBtn.bind(this)
		});
		container.append(this.saveBtn);
	};


	LevelsDialog.prototype.onEditBtn = function(){
		this.editor.editMode(!this.editor.edit);
	};

	LevelsDialog.prototype.onNewBtn = function(){
		this.editor.gateway.loadLevel("level_new");
	};

	LevelsDialog.prototype.onSaveBtn = function(){
		var json = this.editor.gateway.saveLevel();
		var file = "tata";
		$.post("/KirbyEditor/php/writeLevel.php", {file:file, data:json}, this.onLevelSaved.bind(this, file));
	};

	LevelsDialog.prototype.onLevelSaved = function(file){
		window.open("/KirbyEditor/php/readLevel.php?file=" + file + "&r="+Math.random(), 'blank_');
	};


	return LevelsDialog;
});