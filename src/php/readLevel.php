<?php


$file = isset($_REQUEST['file']) ? $_REQUEST['file'] : "test";

$filepath = dirname(__FILE__) . "/../levels/" . $file;
$data = file_get_contents($filepath);

header('Content-Type: application/json');
echo $data;

?>