define("Editor/MainMenu", ["Editor/commons/Section", "Editor/commons/Button", "Editor/ElementsPanel", "Editor/PropertiesPanel", "Arstider/Engine", "Arstider/Mouse"], function(Section, Button, ElementsPanel, PropertiesPanel, Engine, Mouse){

    var
        PAUSED = false,
        PLAYING = true
    ;

	function MainMenu(){
		Arstider.Super(this, Section, {id:"rightMenu"});

        var thisRef = this;

        this.topSection = new Section({
            className:"topSection",
        });
        this.addChild(this.topSection);

        //Toggle state button
        this.toggleStateButton = new Button({
            id:"toggleStateButton",
            bind:function(){thisRef.toggleState.apply(thisRef);},
            label:"stop"
        });
        this.topSection.addChild(this.toggleStateButton);

        this.tabHolder = new Section({
            className:"tabHolder"
        });
        this.addChild(this.tabHolder);
        //Toggle state button
        this.showElementsPanel = new Button({
            id:"elementsPanelTab",
            className:"tabButton",
            bind:function(){thisRef.changeTab.apply(thisRef, ["elementsPanel"]);},
            label:"Elements"
        });
        this.tabHolder.addChild(this.showElementsPanel);
        this.showPropertiesPanel = new Button({
            id:"propertiesPanelTab",
            className:"tabButton",
            bind:function(){thisRef.changeTab.apply(thisRef, ["propertiesPanel"]);},
            label:"Properties"
        });
        this.tabHolder.addChild(this.showPropertiesPanel);

        this.panelContainer = new Section({
            className:"panelContainer"
        });
        this.addChild(this.panelContainer);

        this.propertiesPanel = new PropertiesPanel();
        this.panelContainer.addChild(this.propertiesPanel);
        this.elementsPanel = new ElementsPanel();
        this.panelContainer.addChild(this.elementsPanel);

        this.currentTab = null;
        this.changeTab("elementsPanel");

        this.update();

        this._overrideInput = function(e, target){

            if(!target){
                Arstider.__cancelBubble = {};
                target = Engine.currentScreen;
            }

            var
                i,
                u,
                numInputs = 1
            ;

            if(target && target.children && target.children.length > 0){
                for(i = target.children.length-1; i>=0; i--){
                    if(target && target.children && target.children[i] && !target.children[i].__skip){
                        for(u=0; u<numInputs;u++){
                            if(Arstider.__cancelBubble[u] !== true){
                                if(target.children[i].isTouched(Mouse.x(u), Mouse.y(u))){
                                    if(Mouse.isPressed(u)){
                                        thisRef.selectElement(target.children[i]);
                                    }
                                    break;
                                }
                            }
                        }

                        //recursion
                        if(target && target.children && target.children[i] && !target.children[i].__skip && target.children[i].children && target.children[i].children.length > 0) thisRef._overrideInput(e, target.children[i]);
                    }
                }
            }

            Arstider.__cancelBubble = {};
            i = u = numInputs = null;
        };

        this.unselectRecursive = function(e){
            if(e){
                if(e.__selected){
                    e.__selected = false;
                    e.showOutline = false;
                }

                if(e.children && e.children.length > 0){
                    for(i = e.children.length-1; i>=0; i--){

                    }
                }
            }
        }

	}
	Arstider.Inherit(MainMenu, Section);

    MainMenu.prototype.init = function(){

    };

    MainMenu.prototype.unselectAllElement = function(o){
        if(!o){
            return;
        }

        o.__selected = false;
        o.showOutline = false;

        if(o.children){
            for (var i = o.children.length - 1; i >= 0; i--) {
                this.unselectAllElement(o.children[i]);
            };
        }
    }

    MainMenu.prototype.selectElement = function(e){
        this.unselectAllElement(Engine.currentScreen);

        if(gateway.entityFilter(e) && !e.__selected){
            //console.log("selected ",e);
            Arstider.cancelBubble();
            e.showOutline = true;
            e.__selected = true;
            this.changeTab("propertiesPanel");
            this.propertiesPanel.select(e);
        }

    };

    MainMenu.prototype.toggleState = function(){
        //console.log("changing state!");
        Arstider.skipUpdate = !Arstider.skipUpdate;

        this.update();
    };

    MainMenu.prototype.update = function(){
        this.engineState = !Arstider.skipUpdate;

        if(this.engineState === PLAYING){
            //console.log("can stop");
            Mouse._touchRelay = Engine.applyTouch;
        }
        else{
            //console.log("can play");
            Mouse._touchRelay = this._overrideInput;
        }

        this.toggleStateButton.changeLabel((this.engineState === PLAYING)?"stop":"play");
    };

    MainMenu.prototype.changeTab = function(tab){
        if(this.currentTab == tab) return;

        for(var i = 0; i< this.panelContainer.children.length; i++){
            if(this.panelContainer.children[i].tag) this.panelContainer.children[i].tag.style.display = "none";
        }
        this.panelContainer.get(tab).tag.style.display = "block";
    };

	return MainMenu;

});